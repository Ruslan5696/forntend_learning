/**
 * @param {String} tweet
 * @returns {String[]}
 */
module.exports = function (tweet) {
    let result = tweet.split(" ").filter(word => word.startsWith("#")).map(it => it.slice(1));
    console.log(result);
    return result

};
