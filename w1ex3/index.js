/**
 * @param {Number} hours
 * @param {Number} minutes
 * @param {Number} interval
 * @returns {String}
 */
module.exports = function (hours, minutes, interval) {
    var newMinutes = (minutes + interval) % 60;
    var newHours = (hours + Math.floor((minutes + interval) / 60)) % 24;
    let result = (newHours >= 10 ? newHours : '0' + newHours) + ':' + (newMinutes >= 10 ? newMinutes : '0' + newMinutes);
    console.log(result)
    return result
};
