/**
 * @param {String[]} hashtags
 * @returns {String}
 */
module.exports = function (hashtags) {

    return hashtags
        .reduce((acc, current) => {
            if (acc.indexOf(current.toLowerCase()) < 0) {
                acc.push(current.toLowerCase())
            }
            return acc;
        }, [])
        .join(", ").trim();

};
