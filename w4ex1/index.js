/**
 * @param {Array} collection
 * @params {Function[]} – Функции для запроса
 * @returns {Array}
 */

function logThis() {
    console.log(this)
}

this.logFunc = logThis;

function query(collection) {



    var g = {
        str: "str22",
        mStr: "mstr",
        some: function () {
        },
        move: function () {
            let eva = eval;
            logThis.bind(this)()
        }
    };
    g.move();


    let argArray = [].slice.call(arguments);
    let objects = argArray[0].slice();

    let functions = argArray.reduce((acc, it) => {
        if (it.name === "cutFields") {
            acc.selects.push(it)
        }
        if (it.name === "filterObjects") {
            acc.filters.push(it)
        }
        return acc;
    }, {selects: [], filters: []});
    console.log(functions);

    let filteredObjects = objects;
    functions.filters.forEach(it => {
        filteredObjects = it(filteredObjects)
    })

    let cutedObjects = filteredObjects;
    functions.selects.forEach(it => {
        cutedObjects = it(cutedObjects)
    })
    console.log(cutedObjects);
    return cutedObjects;
}

/**
 * @params {String[]}
 */
function select() {
    let fields = [].slice.call(arguments);
    return function cutFields(objects) {
        return objects.map(it => {
            return copyObjectWithSpecifyFields(it, fields);
        })
    }

    function copyObjectWithSpecifyFields(sourceObject, fields) {
        var newObject = {}
        fields.forEach(it => {
            if (sourceObject.hasOwnProperty(it)) {
                newObject[it] = sourceObject[it];
            }
        })

        return newObject;
    }
}

/**
 * @param {String} property – Свойство для фильтрации
 * @param {Array} values – Массив разрешённых значений
 */
function filterIn(property, values) {
    return function filterObjects(objects) {
        return objects.filter(it => {
            if (it.hasOwnProperty(property)) {
                return values.indexOf(it[property]) >= 0;
            } else {
                return false;
            }
        })
    }

}

module.exports = {
    query: query,
    select: select,
    filterIn: filterIn
};
