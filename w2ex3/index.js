// Телефонная книга
var phoneBook = {};

/**
 * @param {String} command
 * @returns {*} - результат зависит от команды
 */
module.exports = function (command) {
    let argumetnts = command.split(" ");
    switch (argumetnts[0]) {
        case "ADD": {
            add(argumetnts[1], argumetnts[2])
        }
        case "REMOVE_PHONE": {
            return remove(argumetnts[1]);
        }
        case "SHOW": {
            let result = show();
            console.log(result);
            return result;
        }
    }

};

function show() {
    return Object.getOwnPropertyNames(phoneBook).map(it => {
        return (it + ": " + phoneBook[it].join(", ")).trim()
    }).filter(it => !it.endsWith(":")).sort()
}

function remove(phone) {
    var isDeleted = false;
    Object.getOwnPropertyNames(phoneBook).forEach(it => {
        let indexOfNumber = phoneBook[it].indexOf(phone);
        if (indexOfNumber >= 0) {
            phoneBook[it].splice(indexOfNumber, 1)
            isDeleted = true;
        }
    })
    return isDeleted;
}

function add(name, phonesSource) {
    let phones = phonesSource.split(",");
    if (phoneBook.hasOwnProperty(name)) {
        phoneBook[name] = phoneBook[name].concat(phones).reduce((acc, currnet) => {
            if (acc.indexOf(currnet) < 0) {
                acc.push(currnet)
            }
            return acc;
        }, [])
    } else {
        phoneBook[name] = phones
    }
}
