/**
 * @param {String} date
 * @returns {Object}
 */
module.exports = function (date) {
    let regExp = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})/;
    let partsOfDate = date.match(regExp);
    var minutes = partsOfDate[5];
    var hours = partsOfDate[4];
    var days = partsOfDate[3];
    var months = partsOfDate[2];
    var year = partsOfDate[1];

    return myDate = {


        _dateObject: new Date(year, months - 1, days, hours, minutes),
        _value: null,
        get value() {
            let result = this._dateObject.getFullYear() + "-" + formatWithZero(this._dateObject.getMonth()+1) + "-" + formatWithZero(this._dateObject.getDate()) + " " + formatWithZero(this._dateObject.getHours()) + ":" + formatWithZero(this._dateObject.getMinutes());
            console.log(result);
            return result
        },
        add: function (value, block) {
            checkPositive(value);
            safetyAdd(value, block, this._dateObject);
            return this;

        },
        subtract: function (value, block) {
            checkPositive(value);
            safetyAdd(value * -1, block, this._dateObject);
            return this;
        },
    }

};

function formatWithZero(value) {
    return value >= 10 ? value : "0" + value;
}

function checkPositive(value) {
    if (Number(value) < 0) {
        throw new TypeError('Передано неверное значение');
    }
}

function safetyAdd(value, block, date) {
    switch (block) {
        case "years": {
            date.setFullYear(date.getFullYear() + Number(value));
            break;
        }
        case "months": {
            date.setMonth(date.getMonth() + Number(value));
            break;
        }
        case "days": {
            date.setDate(date.getDate() + Number(value));
            break;
        }
        case "hours": {
            date.setHours(date.getHours() + Number(value));
            break;
        }
        case "minutes": {
            date.setMinutes(date.getMinutes() + Number(value));
            break;
        }
        default: {
            throw new TypeError('Передано неверное значение');
        }
    }
}
